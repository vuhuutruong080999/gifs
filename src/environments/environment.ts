// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'gif-social-media',
    appId: '1:91393819691:web:55db14add2520fa734d66e',
    storageBucket: 'gif-social-media.appspot.com',
    apiKey: 'AIzaSyAIjjsxeUsvarjzyflOsTWRtZXV0tyUd1s',
    authDomain: 'gif-social-media.firebaseapp.com',
    messagingSenderId: '91393819691',
  },
  gif_api: 'https://api.giphy.com/v1/gifs',
  gif_api_key: 'tQDLVmghJ33T8p7JSjaZC4xgG1L0WNPz',
  production: false,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
