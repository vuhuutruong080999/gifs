import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GifModule } from './gif/gif.module';
import { ListModule } from './gif/list/list.module';
import { NgxsModule } from '@ngxs/store';
import { DetailModule } from './gif/detail/detail.module';
import { GifViewsState } from './gif/store/state';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { FacebookModule } from 'ngx-facebook';
import { NavbarModule } from './navbar/navbar.module';
import { DarkModeState } from './navbar/store';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxsModule.forRoot([GifViewsState, DarkModeState]),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    GifModule,
    ListModule,
    DetailModule,
    FacebookModule.forRoot(),
    NavbarModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
