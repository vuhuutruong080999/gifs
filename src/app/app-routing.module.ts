import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'gif',
    loadChildren: () => import('./gif/gif.module').then((m) => m.GifModule),
  },
  { path: '', redirectTo: 'gif', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
