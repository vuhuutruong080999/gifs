import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListRoutingModule } from './list-routing.module';
import { ListComponent } from './list.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { ItemGifModule } from './shared/item-gif/item-gif.module';

const MATMODULES = [
  ReactiveFormsModule,
  FormsModule,
  MatButtonModule,
  MatInputModule,
];
@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    ListRoutingModule,
    InfiniteScrollModule,
    ItemGifModule,
    ...MATMODULES,
  ],
})
export class ListModule {}
