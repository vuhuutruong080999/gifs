import { LoadGifs, SearchGifs, ClearSelected } from './../store/actions';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Store } from '@ngxs/store';
import { GifViewsState } from '../store/state';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit, OnDestroy {
  gifs$ = this.store.select(GifViewsState.gifs);
  searchValue = '';
  searchControl = new FormControl();
  constructor(private store: Store) {}
  ngOnDestroy(): void {
    this.store.dispatch(new ClearSelected());
  }

  ngOnInit(): void {
    this.store.dispatch(new LoadGifs(false));
    this.searchControl.valueChanges
      .pipe(distinctUntilChanged(), debounceTime(350))
      .subscribe((res) => {
        this.searchValue = res;
        if (res !== '') this.store.dispatch(new SearchGifs(res, false));
        else this.store.dispatch(new LoadGifs(false));
      });
  }
  loadMore() {
    if (this.searchControl.value !== '') {
      this.store.dispatch(new SearchGifs(this.searchControl.value, true));
    } else {
      this.store.dispatch(new LoadGifs(true));
    }
  }
}
