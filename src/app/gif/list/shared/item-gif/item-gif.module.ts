import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemGifComponent } from './item-gif.component';

@NgModule({
  declarations: [ItemGifComponent],
  imports: [CommonModule],
  exports: [ItemGifComponent],
})
export class ItemGifModule {}
