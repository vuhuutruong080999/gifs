import { STATE_NAME } from './state.model';

export const ActionTypes = {
  LOAD_GIFS: `[${STATE_NAME}] Load gifs`,
  LOAD_GIF_BY_ID: `[${STATE_NAME}] Load gif by ID`,
  SEARCH_GIFS: `[${STATE_NAME}] Search gifs`,
  CLEAR_SELECTED: `[${STATE_NAME}] Clear selected gif`,
};

export class LoadGifs {
  static readonly type = ActionTypes.LOAD_GIFS;
  constructor(public readonly loadMore?: boolean) {}
}

export class LoadGifById {
  static readonly type = ActionTypes.LOAD_GIF_BY_ID;
  constructor(public readonly id: string) {}
}

export class SearchGifs {
  static readonly type = ActionTypes.SEARCH_GIFS;
  constructor(
    public readonly params: string,
    public readonly loadMore?: boolean
  ) {}
}

export class ClearSelected {
  static readonly type = ActionTypes.CLEAR_SELECTED;
}
