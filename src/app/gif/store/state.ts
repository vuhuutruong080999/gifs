import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { STATE_NAME, StateModel } from './state.model';
import { Action, State, StateContext, Selector } from '@ngxs/store';
import { LoadGifs, SearchGifs, LoadGifById, ClearSelected } from './actions';
import { tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { MultiResponse, SingleResponse } from 'giphy-api';
const GIF_API = environment.gif_api;
const GIPHY_API_KEY = environment.gif_api_key;
@State<StateModel>({
  name: STATE_NAME,
})
@Injectable()
export class GifViewsState {
  @Selector()
  static gifs({ gifs }: StateModel) {
    return gifs;
  }

  @Selector()
  static selectedGif({ gif }: StateModel) {
    return gif;
  }

  constructor(private http: HttpClient) {}

  @Action(LoadGifs)
  LoadTrendingGifs(
    { getState, patchState }: StateContext<StateModel>,
    { loadMore = false }: LoadGifs
  ) {
    const currentGifs = getState().gifs;
    return this.http
      .get<MultiResponse>(`${GIF_API}/trending`, {
        params: {
          api_key: GIPHY_API_KEY,
          offset: loadMore ? currentGifs?.pagination.count || 0 : 0,
        },
      })
      .pipe(
        tap((gifs) => {
          if (loadMore)
            patchState({
              gifs: {
                data: (currentGifs?.data || []).concat(gifs.data),
                meta: gifs.meta,
                pagination: {
                  ...gifs.pagination,
                  count:
                    (currentGifs?.pagination.count || 0) +
                    gifs.pagination.count,
                },
              },
            });
          else patchState({ gifs });
        })
      );
  }

  @Action(LoadGifById)
  LoadGifById({ patchState }: StateContext<StateModel>, { id }: LoadGifById) {
    return this.http
      .get<SingleResponse>(`${GIF_API}/${id}`, {
        params: { api_key: GIPHY_API_KEY },
      })
      .pipe(tap((gif) => patchState({ gif: gif.data })));
  }

  @Action(SearchGifs)
  SearchGifs(
    { getState, patchState }: StateContext<StateModel>,
    { params, loadMore }: SearchGifs
  ) {
    const currentGifs = getState().gifs;
    return this.http
      .get<MultiResponse>(`${GIF_API}/search`, {
        params: {
          q: params,
          api_key: GIPHY_API_KEY,
          offset: loadMore ? currentGifs?.pagination.count || 0 : 0,
        },
      })
      .pipe(
        tap((gifs) => {
          if (loadMore)
            patchState({
              gifs: {
                data: (currentGifs?.data || []).concat(gifs.data),
                meta: gifs.meta,
                pagination: {
                  ...gifs.pagination,
                  count:
                    (currentGifs?.pagination.count || 0) +
                    gifs.pagination.count,
                },
              },
            });
          else patchState({ gifs });
        })
      );
  }

  @Action(ClearSelected)
  ClearSelectedGif({ patchState }: StateContext<StateModel>) {
    patchState({ gifs: undefined, gif: undefined });
  }
}
