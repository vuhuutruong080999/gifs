import { GIFObject, MultiResponse } from 'giphy-api';

export const STATE_NAME = 'Gifs';

export interface StateModel {
  gifs?: MultiResponse;
  gif?: GIFObject;
}
