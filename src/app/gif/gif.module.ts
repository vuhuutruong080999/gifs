import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GifRoutingModule } from './gif-routing.module';
import { NgxsModule } from '@ngxs/store';
import { GifViewsState } from './store';
import { GifComponent } from './gif.component';

@NgModule({
  declarations: [
    GifComponent
  ],
  imports: [
    CommonModule,
    GifRoutingModule,
    NgxsModule.forFeature([GifViewsState]),
  ],
})
export class GifModule {}
