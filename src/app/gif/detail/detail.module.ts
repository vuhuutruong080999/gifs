import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailRoutingModule } from './detail-routing.module';
import { DetailComponent } from './detail.component';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { FileSizePipe } from './shared/pipes/filesize.pipe';
import { ItemGifModule } from './shared/pipes/item-gif/item-gif.module';

@NgModule({
  declarations: [DetailComponent, FileSizePipe],
  imports: [
    CommonModule,
    DetailRoutingModule,
    ClipboardModule,
    ItemGifModule,
    MatSnackBarModule,
  ],
})
export class DetailModule {}
