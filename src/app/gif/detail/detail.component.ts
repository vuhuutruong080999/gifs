import { GifViewsState } from './../store/state';
import { Store } from '@ngxs/store';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ClearSelected, LoadGifById, SearchGifs } from '../store/actions';
import { Clipboard } from '@angular/cdk/clipboard';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { FacebookService, UIParams, UIResponse } from 'ngx-facebook';
import { saveAs } from 'file-saver';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit, OnDestroy {
  gif$ = this.store.select(GifViewsState.selectedGif);
  relatedGifs$ = this.store.select(GifViewsState.gifs);
  constructor(
    private store: Store,
    private clipboard: Clipboard,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private fb: FacebookService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id') || '';
    this.loadData(id);
    this.router.events
      .pipe(filter((event) => event instanceof NavigationStart))
      .subscribe((event: any) => {
        const last =
          location.href.split('/')[location.href.split('/').length - 1];
        if (last === 'list') {
        } else if (event.navigationTrigger === 'popstate') {
          window.scrollTo(0, 0);
          this.store.dispatch(new ClearSelected());
          this.loadData(last);
        }
      });
  }
  loadData(id: string) {
    this.store.dispatch(new LoadGifById(id)).subscribe((res) => {
      const q = res?.Gifs.gif.title.slice(0, 20) || '';
      this.store.dispatch(new SearchGifs(q));
    });
  }
  ngOnDestroy(): void {
    this.store.dispatch(new ClearSelected());
  }
  copy(url: string) {
    this.clipboard.copy(url);
    this.openSnackBar('Copied!');
  }
  goTo(id: string) {
    this.router.navigate(['/gif/detail', id]);
    this.store.dispatch(new ClearSelected());
    this.loadData(id);
    window.scrollTo(0, 0);
  }
  share(url: string) {
    const params: UIParams = {
      href: url,
      method: 'share',
    };
    this.fb
      .ui(params)
      .then((res: UIResponse) => this.openSnackBar('Shared!'))
      .catch((e: any) => console.error(e));
  }
  saveFile(name: string, url: string) {
    saveAs(url, name);
    this.openSnackBar('Downloaded');
  }
  openSnackBar(message: string) {
    this.snackBar.open(message, undefined, {
      duration: 2000,
      panelClass: 'style-success',
    });
  }
  faildSnackBar(message: string) {
    this.snackBar.open(message, undefined, {
      duration: 2000,
      panelClass: 'style-faild',
    });
  }
}
