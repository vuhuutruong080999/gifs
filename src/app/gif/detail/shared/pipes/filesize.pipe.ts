import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filesize',
})
export class FileSizePipe implements PipeTransform {
  transform(size: string) {
    const kb = parseInt(size) / 1024;
    let sizeShow = kb.toFixed(2) + 'KB';
    if (kb >= 1024) {
      const mb = kb / 1024;
      sizeShow = mb.toFixed(2) + 'MB';
    }
    return sizeShow;
  }
}
