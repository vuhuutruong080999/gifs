import { Component, Input, OnInit } from '@angular/core';
import { GIFObject } from 'giphy-api';

@Component({
  selector: 'item-gif',
  templateUrl: './item-gif.component.html',
  styleUrls: ['./item-gif.component.scss'],
})
export class ItemGifComponent implements OnInit {
  @Input() gif!: GIFObject;
  hover = false;
  constructor() {}

  ngOnInit(): void {}
  changeHover() {
    this.hover = !this.hover;
  }
}
