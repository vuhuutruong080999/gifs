import { Component } from '@angular/core';
import { FacebookService, InitParams } from 'ngx-facebook';

@Component({
  selector: 'app-root',
  template: '<navbar></navbar><router-outlet></router-outlet>',
})
export class AppComponent {
  title = 'gifs-social-media';
  constructor(private fb: FacebookService) {
    const params: InitParams = {
      appId: '387451686556762',
      xfbml: true,
      version: 'v13.0',
    };
    fb.init(params);
  }
}
