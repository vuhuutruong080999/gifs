import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { DarkModeState } from './store';
import { SetMode } from './store/actions';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  mode$ = this.store.select(DarkModeState.mode);
  constructor(private store: Store) {}

  ngOnInit(): void {}
  changeMode(mode: 'light' | 'dark') {
    this.store.dispatch(new SetMode(mode === 'dark' ? 'light' : 'dark'));
  }
}
