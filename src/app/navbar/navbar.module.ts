import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar.component';
import { RouterModule } from '@angular/router';
import { NgxsModule } from '@ngxs/store';
import { DarkModeState } from './store';

@NgModule({
  declarations: [NavbarComponent],
  imports: [CommonModule, RouterModule, NgxsModule.forFeature([DarkModeState])],
  exports: [NavbarComponent],
})
export class NavbarModule {}
