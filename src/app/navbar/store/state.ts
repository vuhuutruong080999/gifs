import { DOCUMENT } from '@angular/common';
import { Injectable, Inject } from '@angular/core';
import { State, NgxsOnInit, Selector, StateContext, Action } from '@ngxs/store';
import { SetMode } from './actions';
import { StateModel, STATE_NAME } from './state.model';

@State<StateModel>({
  name: STATE_NAME,
})
@Injectable()
export class DarkModeState implements NgxsOnInit {
  @Selector()
  static mode({ mode }: StateModel) {
    return mode;
  }

  constructor(@Inject(DOCUMENT) private document: Document) {}

  ngxsOnInit({ getState, dispatch }: StateContext<StateModel>) {
    const darkModePrefer = this.document.defaultView?.matchMedia(
      '(prefers-color-scheme: dark)'
    ).matches
      ? 'dark'
      : 'light';
    dispatch(new SetMode(getState().mode ?? darkModePrefer));
  }

  @Action(SetMode)
  SetMode({ patchState }: StateContext<StateModel>, { mode }: SetMode) {
    if (mode === 'dark') this.document.body.classList.add('dark');
    else this.document.body.classList.remove('dark');
    patchState({ mode });
  }
}
