export const STATE_NAME = 'DarkMode';

export interface StateModel {
  mode: 'light' | 'dark';
}
