# GIFs

## Public URL: https://gif-social-media.web.app

## Technologies

- [Angular](https://angular.io/) (12.2.0)
- [NGXS](https://www.ngxs.io/) (3.7.3-dev.master-33cace6) for state management.

## Features Overview

- API: [@types/giphy-api](https://www.npmjs.com/package/@types/giphy-api)
- List trending gifs with searchable
- Infinite-scrolled: [ngx-infinite-scroll](https://www.npmjs.com/package/ngx-infinite-scroll)
- View detail gif
- Copy link to clipboard: [angular/cdk/clipboard](https://www.npmjs.com/package/@angular/cdk)
- Share on Facebook: [ngx-facebook](https://www.npmjs.com/package/ngx-facebook)
- Download gif to device: [file-saver](https://www.npmjs.com/package/file-saver)
- CI/CD supported by Firebase
- Dark-mode supported
